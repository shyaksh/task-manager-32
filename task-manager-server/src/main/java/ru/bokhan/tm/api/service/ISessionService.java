package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    void close(@NotNull final SessionDTO session);

    void closeAll(@NotNull final SessionDTO session);

    @Nullable
    UserDTO getUser(@NotNull final SessionDTO session);

    @NotNull
    String getUserId(@NotNull final SessionDTO session);

    @NotNull
    List<SessionDTO> findAll(@NotNull SessionDTO session);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    boolean isValid(@NotNull final SessionDTO session);

    void validate(@NotNull final SessionDTO session);

    void validate(@NotNull final SessionDTO session, @Nullable Role role);

    @Nullable
    SessionDTO open(@NotNull final String login, @NotNull final String password);

    boolean checkDataAccess(@Nullable final String login, @Nullable final String password);

    void signOutByLogin(@Nullable final String login);

    void signOutByUserId(@Nullable final String userId);

}
