package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectDTO> {

    void clear(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @Nullable
    ProjectDTO findById(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findByName(@NotNull String userId, @NotNull String name);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void removeByName(@NotNull String userId, @NotNull String name);

}