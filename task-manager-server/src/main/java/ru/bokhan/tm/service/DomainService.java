package ru.bokhan.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.dto.Domain;
import ru.bokhan.tm.exception.system.FileOperationException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;

@Service
public final class DomainService implements IDomainService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void save(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
        domain.setTasks(taskService.findAll());
    }

    @Override
    public boolean saveToXml() {
        @NotNull final Domain domain = new Domain();
        save(domain);
        @NotNull final File file = new File(DataConstant.FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
        } catch (IOException e) {
            throw new FileOperationException(e.getMessage());
        }
        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean loadFromXml() {
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_XML)) {
            @NotNull final ObjectMapper mapper = new XmlMapper();
            @NotNull final Domain domain = mapper.readValue(fileInputStream, Domain.class);
            load(domain);
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean removeXml() {
        @NotNull final File file = new File(DataConstant.FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean saveToJson() {
        @NotNull final Domain domain = new Domain();
        save(domain);
        @NotNull final File file = new File(DataConstant.FILE_JSON);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
        } catch (IOException e) {
            throw new FileOperationException(e.getMessage());
        }
        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean loadFromJson() {
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_JSON)) {
            @NotNull final ObjectMapper mapper = new ObjectMapper();
            @NotNull final Domain domain = mapper.readValue(fileInputStream, Domain.class);
            load(domain);
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean removeJson() {
        @NotNull final File file = new File(DataConstant.FILE_JSON);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean saveToBinary() {
        @NotNull final Domain domain = new Domain();
        save(domain);
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
        } catch (IOException e) {
            throw new FileOperationException(e.getMessage());
        }
        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
        ) {
            objectOutputStream.writeObject(domain);
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean loadFromBinary() {
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY)) {
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            load(domain);
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean removeBinary() {
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean saveToBase64() {
        @NotNull final Domain domain = new Domain();
        save(domain);
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
        } catch (IOException e) {
            throw new FileOperationException(e.getMessage());
        }
        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
                @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)
        ) {
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = new BASE64Encoder().encode(bytes);
            fileOutputStream.write(base64.getBytes());
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    @SneakyThrows
    public boolean loadFromBase64() {
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        @NotNull final byte[] fileBytes = Files.readAllBytes(file.toPath());
        @NotNull final String base64 = new String(fileBytes);
        @NotNull final byte[] bytes = new BASE64Decoder().decodeBuffer(base64);
        try (
                @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            load(domain);
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean removeBase64() {
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            throw new FileOperationException(e.getMessage());
        }
        return true;
    }

}
