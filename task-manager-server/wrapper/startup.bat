@If Not Exist "..\log" (
mkdir ..\log
)

@echo TASK MANAGER SERVER IS RUNNING...
@java -jar ./task-manager-server.jar > ../log/tm-server.log 2>&1