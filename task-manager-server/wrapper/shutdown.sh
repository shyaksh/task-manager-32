#!/bin/bash
PORT="8090"
if [ -n "$1" ]; then
  PORT=$1
fi

FILENAME="tm-server-$PORT"
echo "SHUTDOWN SERVER AT PORT: $PORT..."
if [ ! -f ./"$FILENAME".pid ]; then
  echo "$FILENAME.pid not found"
  exit 1
fi

echo "KILL PROCESS WITH PID "$(cat ./"$FILENAME".pid)
kill -9 $(cat ./"$FILENAME".pid)
rm ./"$FILENAME".pid
echo "OK"
