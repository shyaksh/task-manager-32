package ru.bokhan.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.endpoint.UserEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;
import ru.bokhan.tm.util.TerminalUtil;

@Component
public final class RegistryListener extends AbstractListener {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String command() {
        return "registry";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registry";
    }

    @Override
    @EventListener(condition = "@registryListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL:]");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        userEndpoint.registerUserWithEmail(login, password, email);
        System.out.println("[OK:]");
    }

}

